/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pkraciuk;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 *
 * @author Piotr
 */
public class ScoresActivity extends Activity {

    Button backButton;
    TextView name1;
    TextView name2;
    TextView name3;
    TextView name4;
    TextView name5;
    TextView name6;
    Line[] lineList;
    Player[] playerList;
    BestScores bestScores;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.scores);
        bestScores = new BestScores(getApplicationContext());
        Bundle b = getIntent().getExtras();
        final int score = b.getInt("score");
        playerList = bestScores.loadScores();
        lineList = new Line[6];
        lineList[0] = addLine(R.id.line1, R.id.name1, R.id.score1, R.id.line1a, R.id.name1a, R.id.score1a, 0);
        lineList[1] = addLine(R.id.line2, R.id.name2, R.id.score2, R.id.line2a, R.id.name2a, R.id.score2a, 1);
        lineList[2] = addLine(R.id.line3, R.id.name3, R.id.score3, R.id.line3a, R.id.name3a, R.id.score3a, 2);
        lineList[3] = addLine(R.id.line4, R.id.name4, R.id.score4, R.id.line4a, R.id.name4a, R.id.score4a, 3);
        lineList[4] = addLine(R.id.line5, R.id.name5, R.id.score5, R.id.line5a, R.id.name5a, R.id.score5a, 4);
        lineList[5] = addLine(R.id.line6, R.id.name6, R.id.score6, R.id.line6a, R.id.name6a, R.id.score6a, 5);
        addToTable(score);

        backButton = (Button) findViewById(R.id.scoresb);
        backButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                backButton.setBackgroundResource(R.drawable.button2a);
                Intent intent = new Intent(view.getContext(), MenuActivity.class);
                startActivity(intent);
                finish();
            }
        });



    }

    private Line addLine(int rl1, int n1, int p1, int rl2, int n2, int p2, final int i) {
        Line line = new Line();
        line.relativeLayout1 = (RelativeLayout) findViewById(rl1);
        line.name = (TextView) findViewById(n1);
        line.name.setText(playerList[i].name);
        line.score = (TextView) findViewById(p1);
        line.score.setText(playerList[i].score);
        line.relativeLayout2 = (RelativeLayout) findViewById(rl2);
        line.nameEntry = (EditText) findViewById(n2);
        line.newScore = (TextView) findViewById(p2);
        //   line.nameEntry.setOnTouchListener(new OnTouchListener(){

        // public boolean onTouch(View view, MotionEvent me) {
        //   lineList[i].nameEntry.setText("");
        //  return true;
        // }

        // });
        line.nameEntry.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
                Log.d("dodawanie", "lineList[i].name.getText()=" + lineList[i].name.getText());
                Log.d("dodawanie", "lineList[i].score.getText()=" + lineList[i].score.getText());
                Log.d("dodawanie", "lineList[i].nameEntry.getText()=" + lineList[i].nameEntry.getText());
                Log.d("dodawanie", "lineList[i].newScore.getText()=" + lineList[i].newScore.getText());


                if (actionId == EditorInfo.IME_ACTION_SEARCH
                        || actionId == EditorInfo.IME_ACTION_DONE
                        || event.getAction() == KeyEvent.ACTION_DOWN
                        && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    //if (!event.isShiftPressed()) {
                    lineList[i].name.setText(lineList[i].nameEntry.getText());
                    lineList[i].score.setText(lineList[i].newScore.getText());
                    lineList[i].relativeLayout2.setVisibility(View.GONE);
                    lineList[i].relativeLayout1.setVisibility(View.VISIBLE);
                    Log.d("dodawanie", "skonczone");
                    
                    InputMethodManager imm = (InputMethodManager) getSystemService(
                            Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(lineList[i].nameEntry.getWindowToken(), 0);
                    
                    //Tworzenie playerListy do zapisu
                    for (int i = 0; i < 6; i++) {
                        playerList[i].name = lineList[i].name.getText().toString();
                        playerList[i].score = lineList[i].score.getText().toString();

                    }
                    bestScores.saveScores(playerList);
                    return true;
                }
                return false;
            }
        });

        return line;
    }

    private void addToTable(int score) {
        int helper = 6;
        String nameHelper = "";
        String scoreHelper = "";
        for (int i = 0; i < 6; i++) {
            if (score > Integer.parseInt(lineList[i].score.getText().toString())) {
                lineList[i].relativeLayout1.setVisibility(View.GONE);
                lineList[i].relativeLayout2.setVisibility(View.VISIBLE);
                lineList[i].newScore.setText(score + "");
                helper = i + 1;
                nameHelper = lineList[i].name.getText().toString();
                scoreHelper = lineList[i].score.getText().toString();
                break;
            }
        }
        if (helper < 6) {
            for (int i = helper; i < 6; i++) {
                String nameHelper2 = lineList[i].name.getText().toString();
                String scoreHelper2 = lineList[i].score.getText().toString();
                lineList[i].name.setText(nameHelper);
                lineList[i].score.setText(scoreHelper);
                nameHelper = nameHelper2;
                scoreHelper = scoreHelper2;
            }
        }
    }

    private class Line {

        RelativeLayout relativeLayout1;
        TextView name;
        TextView score;
        RelativeLayout relativeLayout2;
        EditText nameEntry;
        TextView newScore;
    }
}

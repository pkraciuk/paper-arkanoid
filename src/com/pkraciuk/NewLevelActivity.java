/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pkraciuk;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

/**
 *
 * @author Piotr
 */
public class NewLevelActivity extends Activity{
    
    Button clickButton;
    private int level;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.newlevel);
        
        String info;
        Bundle b = getIntent().getExtras();
        final int level = b.getInt("level");       
        if (level==0){
            info = "Custom Level";
        }
        else{
            info = "Level "+level;
        }
        final int score = b.getInt("score");
        clickButton = (Button) findViewById(R.id.newlevelb);
        clickButton.setText(info);
        clickButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), GameActivity.class);
                Bundle b = new Bundle();
                b.putInt("level", level);
                b.putInt("score", score);
                intent.putExtras(b);
                startActivity(intent);
                finish();
            }
        });
    }
}
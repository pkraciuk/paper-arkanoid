/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pkraciuk;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

/**
 *
 * @author Piotr
 */
public class GameOverActivity extends Activity {
    
    Button clickButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("problem", "tworzę mode1");
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.gameover);        
        Bundle b = getIntent().getExtras();
        int level = b.getInt("level");
        final int score = b.getInt("score");     
        
        Statistics statistics = new Statistics();
        
        if (level>=5){
            statistics.bonus2=true;
        }
        if (level>=10){
            statistics.bonus3=true;
        }
        if (level>=15){
            statistics.bonus4=true;
        }
        if(level>=20){
            statistics.bonus5=true;
        }
        if (level>=25){
            statistics.bonus6=true;
        }        
        statistics.bonus1=true;
        statistics.save(this);
        
        clickButton = (Button) findViewById(R.id.gameoverb);
        clickButton.setText("GAME OVER your score="+score);
        clickButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), ScoresActivity.class);
                Bundle b = new Bundle();
                b.putInt("score",score);
                intent.putExtras(b);
                startActivity(intent);
                finish();
            }
        });
    }
}

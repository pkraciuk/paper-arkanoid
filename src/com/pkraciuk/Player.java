package com.pkraciuk;

import java.io.Serializable;

   

/**
 *
 * @author Piotr
 */
public class Player implements Serializable{

    String name;
    String score;

    public Player(String name, String score) {
        this.name = name;
        this.score = score;
    }
}

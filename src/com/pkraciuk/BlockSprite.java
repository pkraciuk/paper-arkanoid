/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pkraciuk;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;

/**
 *
 * @author Piotr
 */
public class BlockSprite{

    private Bitmap bitmap;
    private Bitmap[] bitmaps;
    private int x;
    private int y;
    private int type;
    private boolean visible;
    private BonusSprite bonus;
    private GameView gameView;
    public void setBonus(BonusSprite bonus){
        this.bonus = bonus;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public BlockSprite(Bitmap[] bitmaps, int x, int y,GameView gameView,int type) {
        this.x = x;
        this.y = y;
        this.visible = true;
        this.type = 1;
        this.bitmaps=bitmaps;       
        this.bonus = null;
        this.gameView = gameView;
        this.type=type;
        this.bitmap=bitmaps[type-1];
    }

    public void setType(int type) {
        this.type = type;
        switch (type) {
            case 1:
                bitmap=bitmaps[0];
                break;
            case 2:
                bitmap=bitmaps[1];
                break;
            case 3:
                bitmap=bitmaps[2];
                break;
            case 4:
                bitmap=bitmaps[3];
                break;
            case 5:
                bitmap=bitmaps[4];
                break;
            case 6:
                bitmap=bitmaps[5];
                break;
        }
    }

    public void draw(Canvas canvas) {
        canvas.drawBitmap(bitmap, x,y,null);
    }

    public Rect getRect() {
        return new Rect(x, y, x + bitmap.getWidth(), y + bitmap.getHeight());
    }

    public void hitBlock() {
        switch (type) {
            case 1:
                setVisible(false);
                if (bonus!=null){
                    bonus.setVisible(true);
                }
                gameView.updateBlockNumber(-1);
                //rl.removeView(this);
                break;
            case 2:
                bitmap=bitmaps[0];
                this.type--;
                break;
            case 3:
                bitmap=bitmaps[1];
                this.type--;
                break;
            case 4:
                bitmap=bitmaps[2];
                this.type--;
                break;
            case 5:
                bitmap=bitmaps[3];
                this.type--;
                break;
            case 6:
                bitmap=bitmaps[4];
                this.type--;
                break;
        }
    }
}

package com.pkraciuk;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;

public class MenuActivity extends Activity {

    private static String TAG = MenuActivity.class.getName();
    Button gameButton;
    Button hiscButton;
    Button editButton;
    Button unlockButton;
    AdView adView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.menu);
        
        Typeface font = Typeface.createFromAsset(getAssets(),"fonts/font.ttf");
        
        adView =(AdView)findViewById(R.id.adView);
        //adView.loadAd(new AdRequest().addTestDevice("60B8970CC9DB86C274D7C7F4B3FE68D9"));
        gameButton = (Button) findViewById(R.id.gameb);
        gameButton.setTypeface(font);
        gameButton.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                gameButton.setBackgroundResource(R.drawable.button1a);
                Intent intent = new Intent(view.getContext(), ModeActivity.class);
                startActivity(intent);
                finish();
            }
        });
        hiscButton = (Button) findViewById(R.id.hiscb);
        hiscButton.setTypeface(font);
        hiscButton.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                hiscButton.setBackgroundResource(R.drawable.button1a);
                Intent intent = new Intent(view.getContext(), ScoresActivity.class);
                Bundle b = new Bundle();
                b.putInt("difficulty", 0);
                intent.putExtras(b);
                startActivity(intent);
                finish();
            }
        });
        editButton = (Button) findViewById(R.id.editb);
        editButton.setTypeface(font);
        editButton.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                editButton.setBackgroundResource(R.drawable.button1a);
                Intent intent = new Intent(view.getContext(), EditorActivity.class);
                Bundle b = new Bundle();
                b.putInt("difficulty", 0);
                intent.putExtras(b);
                startActivity(intent);
                finish();
            }
        });
        unlockButton =(Button) findViewById(R.id.unlb);
        unlockButton.setTypeface(font);
        unlockButton.setOnClickListener(new OnClickListener(){
            public void onClick(View view){
                unlockButton.setBackgroundResource(R.drawable.button1a);
                Intent intent = new Intent(view.getContext(), UnlockablesActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}

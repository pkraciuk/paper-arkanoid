/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pkraciuk;

import android.content.Context;
import android.util.Log;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 *
 * @author Piotr
 */
public class Statistics implements Serializable {

    public boolean bonus1;
    public boolean bonus2;
    public boolean bonus3;
    public boolean bonus4;
    public boolean bonus5;
    public boolean bonus6;
    public int activeLayout;
    public int completeCounter;
    
    public Statistics(){
        this.activeLayout = Levels.activeLayout;
    }
    public void load(Context context) {
        Statistics statistics;
        try {
            FileInputStream fis = context.openFileInput("statistics.data");
            ObjectInputStream is = new ObjectInputStream(fis);
            statistics = (Statistics) is.readObject();
            is.close();
            this.bonus1=statistics.bonus1;
            this.bonus2=statistics.bonus2;
            this.bonus3=statistics.bonus3;
            this.bonus4=statistics.bonus4;
            this.bonus5=statistics.bonus5;
            this.bonus6=statistics.bonus6;
            if (statistics.activeLayout>=0&&statistics.activeLayout<7){
                this.activeLayout=statistics.activeLayout;
            }
            else{
                this.activeLayout=0;
                Log.d("stats","jako aktywny wczytje domyslny");
            }
            Levels.activeLayout=this.activeLayout;
            Log.d("stats","wczytanie udalo sie");
        } catch (Exception e) {
            Log.d("stats", "nie udalo sie");
            this.bonus1 = false;
            this.bonus2 = false;
            this.bonus3 = false;
            this.bonus4 = false;
            this.bonus5 = false;
            this.bonus6 = false;
            this.activeLayout=0;
        }
    }
    public void save(Context context) {
        try {
            FileOutputStream fos = context.openFileOutput("statistics.data", Context.MODE_PRIVATE);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(this);
            Log.d("stats","zapis udal sie");
            os.close();
        } catch (Exception e) {
            Log.d("stats", "zapis nie udal sie");
        }
    }
}

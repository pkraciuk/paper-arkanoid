package com.pkraciuk;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;

public class BonusSprite {

    private Bitmap bitmap;
    private int x;
    private int y;
    private boolean visible;
    private int deviceHeight;
    private GameView gameView;
    private BouncerSprite bouncer;
    private BallSprite ball;
    private int type;
    //Types:
    //1:life
    //2:long
    //3:short
    //4:slow
    //5:fast
    //6:laser

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
        if (!visible){
            gameView.removeFromBonusList(this);
        }
    }

    public BonusSprite(Bitmap bitmap, int x, int y,int deviceHeight,GameView gameView,BouncerSprite bouncer,BallSprite ball,int type) {
        this.x = x;
        this.y = y;
        this.visible = false;
        this.bitmap = bitmap;
        this.deviceHeight = deviceHeight;
        this.gameView = gameView;
        this.bouncer = bouncer;
        this.type = type;
        this.ball = ball;
    }

    public void draw(Canvas canvas) {
        canvas.drawBitmap(bitmap, x, y, null);
    }

    public void update() {
        if (visible) {
            y += 3;
            if (this.getRect().intersect(bouncer.getRect())){
                visible = false;
                if (type==1){
                    gameView.updateLives(1);
                }
                if (type==2){
                    bouncer.setActiveBitmap(1);
                }
                if (type==3){
                    bouncer.setActiveBitmap(2);
                }
                if (type==4){
                    ball.setSpeed(6);
                }
                if (type==5){
                    ball.setSpeed(9);
                }
            }
            if (y>deviceHeight){
                visible=false;
                //gameView.removeFromBonusList(this);          
            }
        }
    }
    public Rect getRect() {
        return new Rect(x, y, x + bitmap.getWidth(), y + bitmap.getHeight());
    }
}

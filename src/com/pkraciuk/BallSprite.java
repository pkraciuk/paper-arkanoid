package com.pkraciuk;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.Log;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

public class BallSprite {

    private Bitmap bitmap;
    private double x;
    private double y;
    private int deviceWidth;
    private int deviceHeight;
    private double xSpeed;
    private double ySpeed;
    private BouncerSprite bouncer;
    private List<BlockSprite> blockList;
    private boolean started;
    private GameView gameView;
    private int hitCounter;
    private double speed = 7;
    private int bouncerOldX;

    public BallSprite(Bitmap bitmap, double x, double y, int deviceWidth, int deviceHeight, BouncerSprite _bouncer, List<BlockSprite> blockList, GameView gameView) {
        this.bitmap = bitmap;
        this.x = _bouncer.getX() + _bouncer.getBitmap().getWidth() / 2 - bitmap.getWidth() / 2;
        this.y = _bouncer.getY() - bitmap.getHeight();
        this.deviceWidth = deviceWidth;
        this.deviceHeight = deviceHeight;
        this.xSpeed = 0;
        this.ySpeed = 0;
        this.bouncer = _bouncer;
        this.blockList = blockList;
        started = false;
        this.gameView = gameView;
        this.hitCounter = 0;
        _bouncer.addPropertyChangeListener(new PropertyChangeListener() {
            public void propertyChange(PropertyChangeEvent pce) {
                setStarted(true);

            }
        });
    }

    public void setSpeed(int speed) {
        xSpeed = xSpeed / this.speed * speed;
        ySpeed = -ySpeed / this.speed * speed;
        this.speed = speed;
    }

    public void draw(Canvas canvas) {
        canvas.drawBitmap(bitmap, (int) (x + 0.5), (int) (y + 0.5), null);
    }

    public void update() {
        if (started) {
            Log.d("speed", "xSpeed = " + xSpeed);
            Log.d("speed", "ySpeed = " + ySpeed);
            x += xSpeed;
            y += ySpeed;
            if (y < 0) {
                ySpeed = Math.abs(ySpeed);
            }
            if (x + bitmap.getWidth() > deviceWidth) {
                xSpeed = -Math.abs(xSpeed);
            }
            if (x < 0) {
                xSpeed = Math.abs(xSpeed);
            }
            if (y + bitmap.getHeight() > deviceHeight) {
                gameView.updateLives(-1);
                putOnBouncer();
            }

            if (hitCounter == 0) {
                if (getRect().intersect(bouncer.getRect())) {
                    if (bouncerOldX < bouncer.getX() && (xSpeed > 0 && xSpeed / speed < 0.8 || xSpeed < 0)) {
                        
                        xSpeed += speed/10;
                        ySpeed = (double) Math.sqrt(speed * speed - xSpeed * xSpeed);
                        Log.d("speed2", "zmiana wartości xSpeed ="+xSpeed+" ySpeed = "+ySpeed);
                    } else if (bouncerOldX > bouncer.getX() && (xSpeed < 0 && xSpeed / speed > -0.8 || xSpeed > 0)) {
                        xSpeed -= speed/10;
                        ySpeed = (double) Math.sqrt(speed * speed - xSpeed * xSpeed);
                        Log.d("speed2", "zmiana wartości xSpeed ="+xSpeed+" ySpeed = "+ySpeed);
                    }
                    ySpeed = -Math.abs(ySpeed);
                    hitCounter = 6;
                }


                for (BlockSprite object : blockList) {
                    if (getRect().intersect(object.getRect()) && object.isVisible()) {
                        hitCounter = 3;
                        object.hitBlock();
                        if (CollisionVertical(getRect(), object.getRect())) {
                            ySpeed = -ySpeed;
                        } else {
                            xSpeed = -xSpeed;
                        }
                    }
                }
            } else {
                hitCounter--;
            }

        }
        bouncerOldX = bouncer.getX();
    }

    public Rect getRect() {
        return new Rect((int) (x + 0.5), (int) (y + 0.5), (int) (x + 0.5) + bitmap.getWidth(), (int) (y + 0.5) + bitmap.getHeight());
    }

    private boolean CollisionVertical(Rect ballRect, Rect objectRect) {
        double r1MiddleX = (ballRect.left + ballRect.right) / 2;
        double r1MiddleY = (ballRect.top + ballRect.bottom) / 2;
        double r2MiddleX = (objectRect.left + objectRect.right) / 2;
        double r2MiddleY = (objectRect.top + objectRect.bottom) / 2;
        if ((Math.abs(r1MiddleX - r2MiddleX) < (Math.abs(r1MiddleY - r2MiddleY)))) {
            return true;
        }
        return false;
    }

    public void putOnBouncer() {
        y = bouncer.getY() - bitmap.getHeight();
        x = bouncer.getX() + bouncer.getBitmap().getWidth() / 2 - bitmap.getWidth() / 2;
        started = false;
    }

    public void setStarted(boolean started) {
        if (!this.started && started) {
            this.started = started;
            this.xSpeed = (double)Math.sqrt((speed*speed)/2)+speed/10;
            this.ySpeed = (double) Math.abs(Math.sqrt(speed * speed - xSpeed * xSpeed));
        }
    }
}

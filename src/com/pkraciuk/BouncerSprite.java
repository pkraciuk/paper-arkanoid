/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pkraciuk;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.List;
public class BouncerSprite{
    
    private int activeBitmap;
    private Bitmap[] bitmaps;
    private int x;
    private int y;
    private TouchmeSprite touchme;
    public PropertyChangeSupport pcs;
    
    public Bitmap getBitmap(){
        return bitmaps[activeBitmap];
    }

    public BouncerSprite(Bitmap[] bitmaps,int x, int y, TouchmeSprite touchme) {
        this.bitmaps = bitmaps;
        this.x = x;
        this.y = y;
        this.touchme = touchme;
        this.activeBitmap = 0;
        pcs = new PropertyChangeSupport(this);
    }
        public void addPropertyChangeListener(PropertyChangeListener listener){
        pcs.addPropertyChangeListener(listener);
    }

    public void draw(Canvas canvas) {
        canvas.drawBitmap(bitmaps[activeBitmap], x,y,null);
    }

    public void update() {
        setX(touchme.getX()+touchme.getBitmap().getWidth()/2 - bitmaps[activeBitmap].getWidth() / 2);
        //for (BonusSprite object:bonusList){
            //if (getRect().intersect(object.getRect())){
                //bonusList.remove(object);
                //break;        
            //}
        //}
    }

    public Rect getRect() {
        return new Rect(x, y, x + bitmaps[activeBitmap].getWidth(), y + bitmaps[activeBitmap].getHeight());
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        pcs.firePropertyChange("x",this.x, x);
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
    
    public void setActiveBitmap(int activeBitmap){
        this.activeBitmap = activeBitmap;
        touchme.setBouncerWidth(bitmaps[activeBitmap].getWidth());
    }
    
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pkraciuk;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author Piotr
 */
public class GameView extends SurfaceView implements SurfaceHolder.Callback {

    private GameThread gameThread;

    public GameThread getGameThread() {
        return gameThread;
    }
    private TouchmeSprite touchme;
    private BallSprite ball;
    private BouncerSprite bouncer;
    private List<BlockSprite> blockList = new ArrayList<BlockSprite>();
    private Bitmap background;
    private int lives;
    private GameActivity gameActivity;
    private List<BonusSprite> bonusList = new ArrayList<BonusSprite>();
    private int blockNumber;
    private int level;
    private int score;
    
    public void updateScore(int update){
        this.score+=update;
    }

    public GameView(Context context, int level, int score) {
        super(context);
        getHolder().addCallback(this);
        this.gameActivity = (GameActivity) context;
        this.level = level;
        this.score = score;
        Log.d("score", score+"");
        //WYMIARY EKRANU
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        int deviceWidth = display.getWidth();
        int deviceHeight = display.getHeight();

        Bitmap bitmap;
        bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.touch);
        bitmap = Bitmap.createScaledBitmap(bitmap, deviceWidth / 6, deviceWidth / 6, false);
        touchme = new TouchmeSprite(bitmap, deviceWidth / 6 - deviceWidth / 12, deviceHeight / 10 * 8 + deviceWidth / (3 * 7) + deviceHeight / 40, deviceWidth / 3, deviceWidth);
        
        Bitmap[] bitmaps = new Bitmap[3];
        
        bitmaps[0] = BitmapFactory.decodeResource(getResources(), R.drawable.bouncer);
        bitmaps[0] = Bitmap.createScaledBitmap(bitmaps[0], deviceWidth / 3, deviceWidth / (3 * 7), false);
        
        bitmaps[1] = BitmapFactory.decodeResource(getResources(), R.drawable.bouncerlon);
        bitmaps[1] = Bitmap.createScaledBitmap(bitmaps[0], deviceWidth / 2, deviceWidth / (3 * 7), false);
        bitmaps[2] = BitmapFactory.decodeResource(getResources(), R.drawable.bouncersho);
        bitmaps[2] = Bitmap.createScaledBitmap(bitmaps[0], deviceWidth / 4, deviceWidth / (3 * 7), false);
        bouncer = new BouncerSprite(bitmaps, 0, deviceHeight / 10 * 8, touchme);

        bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ball);
        bitmap = Bitmap.createScaledBitmap(bitmap, deviceWidth / 20, deviceWidth / 20, false);
        ball = new BallSprite(bitmap, 100.0d, 100.0d, deviceWidth, deviceHeight, bouncer, blockList, this);        
        
        //wczytywnie klockow i bonusów
        int [] levelStructure;
        if (level==0){
            levelStructure = loadLevelStructure(context);
        }
        else{
            levelStructure = Levels.levelList[level-1];
        }
        setBlocksAndBonuses(levelStructure, deviceWidth, deviceHeight);

        //wczytywanie pozostałych spritów




        background = BitmapFactory.decodeResource(getResources(), R.drawable.panel);
        background = Bitmap.createScaledBitmap(background, deviceWidth, deviceHeight, false);

        lives = 3;

        gameThread = new GameThread(getHolder(), this);
        setFocusable(true);
    }

    public void surfaceCreated(SurfaceHolder holder) {
        gameThread.setRunning(true);
        gameThread.start();
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        boolean retry = true;
        while (retry) {
            try {
                gameThread.join();
                retry = false;
            } catch (InterruptedException e) {
            }
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        touchme.update(event);
        if (event.getX() < 50 && event.getY() < 50) {
            gameThread.setRunning(false);
            ((Activity) getContext()).finish();
        }
        return true;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawColor(Color.BLACK);
        canvas.drawBitmap(background, 0, 0, null);
        touchme.draw(canvas);
        ball.draw(canvas);
        bouncer.draw(canvas);
        for (BlockSprite object : blockList) {
            if (object.isVisible()) {
                object.draw(canvas);
            }
        }
        for (BonusSprite object : bonusList) {
            if (object.isVisible()) {
                object.draw(canvas);
            }
        }
    }

    public void update() {
        ball.update();
        bouncer.update();
        for (BonusSprite object : bonusList) {
            object.update();
        }

    }

    public void updateLives(int update) {
        this.lives += update;
        if (lives <= 0) {
            gameThread.setRunning(false);
            Intent intent = new Intent(getContext(), GameOverActivity.class);
            Bundle b = new Bundle();
            b.putInt("level",level);
            b.putInt("score", score);
            intent.putExtras(b);
            gameActivity.startActivity(intent);
            gameActivity.finish();
        }
    }

    public void updateBlockNumber(int update) {
        blockNumber += update;
        updateScore(10);
        if (blockNumber <= 0) {
            gameThread.setRunning(false);
            Intent intent;
            Bundle b = new Bundle();
            if (level==0||level == 5){
                intent = new Intent(getContext(), GameOverActivity.class);
                b.putInt("level",level);
            }
            else {
                intent = new Intent(getContext(),NewLevelActivity.class);
                b.putInt("level",level+1);
            }         
            b.putInt("score", score);
            intent.putExtras(b);
            gameActivity.startActivity(intent);
            gameActivity.finish();
        }
    }

    private int[] loadLevelStructure(Context context) {
        String helper = "";
        int[] levelStructure = new int[54];
        try {
            BufferedReader inputReader = new BufferedReader(new InputStreamReader(context.openFileInput("CustomLevel")));
            String inputString;
            StringBuilder stringBuffer = new StringBuilder();
            while ((inputString = inputReader.readLine()) != null) {
                stringBuffer.append(inputString).append("\n");
            }
            helper = stringBuffer.toString();
        } catch (IOException e) {
            Log.d("wczytywanie", "null pierwszy");
        }
        for (int i = 0; i < 54; i++) {
            try {
                char c = helper.charAt(i);
                int type = Character.getNumericValue(c);
                if (type >= 0 && type < 7) {
                    levelStructure[i] = type;
                } else {
                    Log.d("wczytywanie", "null drugi");
                    return null;
                }
            } catch (Exception e) {
                Log.d("wczytywanie", "null trzeci");
                return null;
            }
        }
        return levelStructure;
    }

    public void removeFromBonusList(BonusSprite bonus) {
        bonusList.remove(bonus);
    }

    private BonusSprite createBonus(int i, int j, int deviceWidth, int deviceHeight) {
        Bitmap bonusBitmap;
        BonusSprite b = null;
        int r = new Random().nextInt(5);
        if (r == 1) {
            int type;
            r = new Random().nextInt(6);

            switch (r) {
                case 1:
                    bonusBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.bonlife);
                    type = 1;
                    break;
                case 2:
                    bonusBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.bonlong);
                    type = 2;
                    break;
                case 3:
                    bonusBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.bonshort);
                    type = 3;
                    break;
                case 4:
                    bonusBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.bonslow);
                    type = 4;
                    break;
                case 5:
                    bonusBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.bonfast);
                    type = 5;
                    break;
                case 6:
                    bonusBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.bonlaser);
                    type = 6;
                    break;
                default:
                    bonusBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.bonlife);
                    type = 1;
                    break;
            }

            bonusBitmap = Bitmap.createScaledBitmap(bonusBitmap, deviceWidth * 8 / 101, deviceWidth * 8 / 101, false);
            b = new BonusSprite(bonusBitmap, deviceWidth * (4 + i * 10 + j % 2 * 5) / 101, deviceWidth * (4 + 10 * j) / 101, deviceHeight, this, bouncer,ball, type);
        }
        return b;
    }

    private void setBlocksAndBonuses(int[] levelStructure, int deviceWidth, int deviceHeight) {

        Bitmap[] bitmaps = new Bitmap[6];
        bitmaps[0] = BitmapFactory.decodeResource(getResources(), R.drawable.block1);
        bitmaps[0] = Bitmap.createScaledBitmap(bitmaps[0], deviceWidth * 8 / 101, deviceWidth * 8 / 101, false);
        bitmaps[1] = BitmapFactory.decodeResource(getResources(), R.drawable.block2);
        bitmaps[1] = Bitmap.createScaledBitmap(bitmaps[1], deviceWidth * 8 / 101, deviceWidth * 8 / 101, false);
        bitmaps[2] = BitmapFactory.decodeResource(getResources(), R.drawable.block3);
        bitmaps[2] = Bitmap.createScaledBitmap(bitmaps[2], deviceWidth * 8 / 101, deviceWidth * 8 / 101, false);
        bitmaps[3] = BitmapFactory.decodeResource(getResources(), R.drawable.block4);
        bitmaps[3] = Bitmap.createScaledBitmap(bitmaps[3], deviceWidth * 8 / 101, deviceWidth * 8 / 101, false);
        bitmaps[4] = BitmapFactory.decodeResource(getResources(), R.drawable.block5);
        bitmaps[4] = Bitmap.createScaledBitmap(bitmaps[4], deviceWidth * 8 / 101, deviceWidth * 8 / 101, false);
        bitmaps[5] = BitmapFactory.decodeResource(getResources(), R.drawable.block6);
        bitmaps[5] = Bitmap.createScaledBitmap(bitmaps[5], deviceWidth * 8 / 101, deviceWidth * 8 / 101, false);
        blockNumber = 0;
        for (int j = 0; j < 6; j++) {
            for (int i = 0; i < 9; i++) {
                final int a = j * 9 + i;
                if (levelStructure == null) {
                    BlockSprite block = new BlockSprite(bitmaps, deviceWidth * (4 + i * 10 + j % 2 * 5) / 101, deviceWidth * (4 + 10 * j) / 101, this, 1);
                    blockList.add(block);
                    blockNumber++;
                    BonusSprite b = createBonus(i, j, deviceWidth, deviceHeight);
                    if (b != null) {
                        bonusList.add(b);
                        block.setBonus(b);
                    }
                } else if (levelStructure[a] != 0) {
                    BlockSprite block = new BlockSprite(bitmaps, deviceWidth * (4 + i * 10 + j % 2 * 5) / 101, deviceWidth * (4 + 10 * j) / 101, this, levelStructure[a]);
                    blockList.add(block);
                    blockNumber++;
                    BonusSprite b = createBonus(i, j, deviceWidth, deviceHeight);
                    if (b != null) {
                        bonusList.add(b);
                        block.setBonus(b);
                    }
                }
            }
        }
    }
}

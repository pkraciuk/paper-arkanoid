package com.pkraciuk;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

/**
 *
 * @author Piotr
 */
public class ModeActivity extends Activity {

    private static String TAG = MenuActivity.class.getName();
    Button standardButton;
    Button customButton;
    Button returnButton;
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            Log.d(this.getClass().getName(), "back button pressed");
                Intent intent = new Intent(ModeActivity.this, MenuActivity.class);
                startActivity(intent);
                finish();
        }
        return super.onKeyDown(keyCode, event);
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("problem", "tworzę difficulty");
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.mode);
        
        Typeface font = Typeface.createFromAsset(getAssets(),"fonts/font.ttf");
        
        standardButton = (Button) findViewById(R.id.standardb);
        standardButton.setTypeface(font);
        standardButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                standardButton.setBackgroundResource(R.drawable.button1a);
                Intent intent = new Intent(view.getContext(), NewLevelActivity.class);
                Bundle b = new Bundle();
                b.putInt("level", 1);
                b.putInt("score",0);
                intent.putExtras(b);
                startActivity(intent);
                finish();
            }
        });
        customButton = (Button) findViewById(R.id.customb);
        customButton.setTypeface(font);
        customButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                customButton.setBackgroundResource(R.drawable.button1a);
                Intent intent = new Intent(view.getContext(), NewLevelActivity.class);
                Bundle b = new Bundle();
                b.putInt("level", 0);
                b.putInt("score",0);
                intent.putExtras(b);
                startActivity(intent);
                finish();
            }
        });
        returnButton = (Button) findViewById(R.id.returnb);
        returnButton.setTypeface(font);
        returnButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                returnButton.setBackgroundResource(R.drawable.button1a);
                Intent intent = new Intent(view.getContext(), MenuActivity.class);
                startActivity(intent);
                finish();
            }
        });


    }
}

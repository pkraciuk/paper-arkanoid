package com.pkraciuk;

import android.content.Context;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ImageView;

/**
 *
 * @author Piotr
 */
public class EditorBlockSprite extends ImageView implements OnTouchListener {

    private int type;
    private boolean isTouched = false;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
        switch (type) {
            case 0:
                setBackgroundResource(R.drawable.block0);
                break;
            case 1:
                setBackgroundResource(R.drawable.block1);
                break;
            case 2:
                setBackgroundResource(R.drawable.block2);
                break;
            case 3:
                setBackgroundResource(R.drawable.block3);
                break;
            case 4:
                setBackgroundResource(R.drawable.block4);
            case 5:
                setBackgroundResource(R.drawable.block5);
                break;
            case 6:
                setBackgroundResource(R.drawable.block6);
                break;
        }
    }

    public EditorBlockSprite(Context context) {
        super(context);
        setBackgroundResource(R.drawable.block1);
        type = 0;
    }

    public boolean onTouch(View view, MotionEvent event) {
        Log.d("problem", "Klikam i co?");
        if (!isTouched && event.getAction()==MotionEvent.ACTION_DOWN) {
            isTouched = true;
            switch (type) {
                case 0:
                    type++;
                    setBackgroundResource(R.drawable.block1);
                    break;
                case 1:
                    type++;
                    setBackgroundResource(R.drawable.block2);
                    break;
                case 2:
                    type++;
                    setBackgroundResource(R.drawable.block3);
                    break;
                case 3:
                    type++;
                    setBackgroundResource(R.drawable.block4);
                    break;
                case 4:
                    type++;
                    setBackgroundResource(R.drawable.block5);
                    break;
                case 5:
                    type++;
                    setBackgroundResource(R.drawable.block6);
                    break;
                case 6:
                    type = 0;
                    setBackgroundResource(R.drawable.block0);
                    break;
            }
        } else if (isTouched) {
            isTouched = false;
        }
        return true;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pkraciuk;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 *
 * @author Piotr
 */
public class UnlockablesActivity extends Activity {

    RelativeLayout rl;
    Button bonusButton1;
    TextView bonusText1;
    Button bonusButton2;
    TextView bonusText2;
    Button bonusButton3;
    TextView bonusText3;
    Button bonusButton4;
    TextView bonusText4;
    Button bonusButton5;
    TextView bonusText5;
    Button bonusButton6;
    TextView bonusText6;
    Button returnButton;
    Statistics statistics;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("problem", "tworzę difficulty");
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.unlockables);
        statistics = new Statistics();
        statistics.load(this);

        rl = (RelativeLayout) findViewById(R.id.unlrl);
        switch (statistics.activeLayout) {
            case 1:
                rl.setBackgroundResource(R.drawable.panel);
                break;
            case 2:
                rl.setBackgroundResource(R.drawable.panel2);
                break;
            case 3:
                rl.setBackgroundResource(R.drawable.panel3);
                break;
            case 4:
                rl.setBackgroundResource(R.drawable.panel4);
                break;
            case 5:
                rl.setBackgroundResource(R.drawable.panel5);
                break;
            case 6:
                rl.setBackgroundResource(R.drawable.panel6);
                break;
        }
        bonusButton1 = (Button) findViewById(R.id.bbut1);
        bonusButton1.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                Levels.activeLayout=1;
                statistics.activeLayout=1;
                rl.setBackgroundResource(R.drawable.panel);
            }
        });
        bonusText1 = (TextView) findViewById(R.id.btxt1);
        if (statistics.bonus1) {
            bonusButton1.setEnabled(true);
            bonusText1.setVisibility(View.INVISIBLE);
        }
        bonusButton2 = (Button) findViewById(R.id.bbut2);
        bonusButton2.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                Levels.activeLayout=2;
                statistics.activeLayout=2;
                rl.setBackgroundResource(R.drawable.panel2);
            }
        });
        bonusText2 = (TextView) findViewById(R.id.btxt2);
        if (statistics.bonus2) {
            bonusButton2.setEnabled(true);
            bonusText2.setVisibility(View.INVISIBLE);
        }
        bonusButton3 = (Button) findViewById(R.id.bbut3);
        bonusText3 = (TextView) findViewById(R.id.btxt3);
        if (statistics.bonus3) {
            bonusButton3.setEnabled(true);
            bonusText3.setVisibility(View.INVISIBLE);
        }
        bonusButton4 = (Button) findViewById(R.id.bbut4);
        bonusText4 = (TextView) findViewById(R.id.btxt4);
        if (statistics.bonus4) {
            bonusButton4.setEnabled(true);
            bonusText4.setVisibility(View.INVISIBLE);
        }
        bonusButton5 = (Button) findViewById(R.id.bbut5);
        bonusText5 = (TextView) findViewById(R.id.btxt5);
        if (statistics.bonus5) {
            bonusButton5.setEnabled(true);
            bonusText5.setVisibility(View.INVISIBLE);
        }
        bonusButton6 = (Button) findViewById(R.id.bbut6);
        bonusText6 = (TextView) findViewById(R.id.btxt6);
        if (statistics.bonus6) {
            bonusButton6.setEnabled(true);
            bonusText6.setVisibility(View.INVISIBLE);
        }

        returnButton = (Button) findViewById(R.id.unlreturn);
        returnButton.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                returnButton.setBackgroundResource(R.drawable.button2a);
                
                statistics.save(view.getContext());


                Intent intent = new Intent(view.getContext(), MenuActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }
}

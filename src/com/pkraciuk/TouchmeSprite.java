/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pkraciuk;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.view.MotionEvent;

/**
 *
 * @author Piotr
 */
public class TouchmeSprite {

    private int x;
    private int y;
    private Bitmap bitmap;
    private boolean touched;
    private int bouncerWidth;
    private int deviceWidth;
    

    public TouchmeSprite(Bitmap bitmap, int x, int y,int bouncerWidth,int deviceWidth) {
        this.bitmap = bitmap;
        this.x = x;
        this.y = y;
        this.bouncerWidth=bouncerWidth;
        this.deviceWidth = deviceWidth;
    }


    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }
    
    public void setBouncerWidth(int bouncerWidth){
        this.bouncerWidth=bouncerWidth;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public boolean isTouched() {
        return touched;
    }

    public void setTouched(boolean touched) {
        this.touched = touched;
    }

    public void draw(Canvas canvas) {
        canvas.drawBitmap(bitmap, x, y, null);
    }

    public void handleActionDown(int eventX, int eventY) {
        if (eventX >= x && (eventX <= (x + bitmap.getWidth()))) {
            if (eventY >= y && (eventY <= (y + bitmap.getHeight()))) {
                setTouched(true);

            } else {
                setTouched(false);
            }
        } else {
            setTouched(false);
        }
    }
    public void update(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            if(event.getX()>=x&&event.getX()<x+bitmap.getWidth()&&event.getY()>=y&&event.getY()<y+bitmap.getHeight()){
                setTouched(true);
            }
            else{
                setTouched(false);
            }
        }
        if (event.getAction() == MotionEvent.ACTION_MOVE) {
            if (isTouched()) {
                if (event.getX()<bouncerWidth/2){
                    setX(bouncerWidth/2-bitmap.getWidth()/2);
                }
                else if(event.getX()>deviceWidth-bouncerWidth/2){
                    setX(deviceWidth-bouncerWidth/2-bitmap.getWidth()/2);
                }
                else{
                    setX((int) event.getX()-bitmap.getWidth()/2);
                }
            }
        }
        if (event.getAction() == MotionEvent.ACTION_UP) {
            if (isTouched()) {
                setTouched(false);
            }
        }

    }
}

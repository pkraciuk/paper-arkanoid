/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pkraciuk;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class EditorActivity extends Activity {

    private Button backButton;
    private Button saveButton;
    private int deviceWidth;
    private int deviceHeight;
    private EditorBlockSprite[] blockList = new EditorBlockSprite[54];

        @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            Log.d(this.getClass().getName(), "back button pressed");
                Intent intent = new Intent(EditorActivity.this, MenuActivity.class);
                startActivity(intent);
                finish();
        }
        return super.onKeyDown(keyCode, event);
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("problem", "tworzę Splash");
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.editor);

        Display display = getWindowManager().getDefaultDisplay();
        deviceWidth = display.getWidth();
        deviceHeight = display.getHeight();

        RelativeLayout rl = (RelativeLayout) findViewById(R.id.editorrl);
        backButton = (Button) findViewById(R.id.backb);
        backButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                backButton.setBackgroundResource(R.drawable.button2a);
                Intent intent = new Intent(view.getContext(), MenuActivity.class);
                startActivity(intent);
                finish();
            }
        });
        saveButton = (Button) findViewById(R.id.saveb);
        saveButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                saveButton.setBackgroundResource(R.drawable.button2a);
                String info = "";
                for (EditorBlockSprite object : blockList) {
                    info += object.getType();
                }
                Log.d("wypisz", info);
                try {
                    FileOutputStream fos = openFileOutput("CustomLevel", Context.MODE_PRIVATE);
                    fos.write(info.getBytes());
                    fos.close();
                    Log.d("zapis","zapis sie udal");
                    Log.d("zapis",info.getBytes().toString());
                } catch (Exception e) {
                    Log.d("zapis", "zapis sie nie udal");
                }
                Intent intent = new Intent(view.getContext(), MenuActivity.class);
                startActivity(intent);
                finish();
            }
        });
        blockList[1] = new EditorBlockSprite(this);
        blockList[1].setOnTouchListener(new OnTouchListener() {
            public boolean onTouch(View view, MotionEvent me) {
                blockList[1].onTouch(view, me);
                return true;
            }
        });
        String levelStructure="";
        try{
            BufferedReader inputReader = new BufferedReader(new InputStreamReader(openFileInput("CustomLevel")));
            String inputString;
            StringBuffer stringBuffer = new StringBuffer();
            while((inputString = inputReader.readLine())!=null){
                stringBuffer.append(inputString +"\n");
            }
            levelStructure=stringBuffer.toString();
            Log.d("wczytanie","nie wywalilo bledu");
        }catch(IOException e){
            Log.d("wczytanie","wywalilo blad");
            e.printStackTrace();
        }
        Log.d("wczytywanie",levelStructure);
        for (int j = 0; j < 6; j++) {
            for (int i = 0; i < 9; i++) {

                final int a = j * 9 + i;
                blockList[a] = new EditorBlockSprite(this);
                try{
                    char b = levelStructure.charAt(a);
                    Log.d("zmiana","zmieniam na "+b);
                    blockList[a].setType(Character.getNumericValue(b));
                }catch(Exception e){ 
                }
                blockList[a].setOnTouchListener(new OnTouchListener() {
                    public boolean onTouch(View view, MotionEvent me) {
                        return blockList[a].onTouch(view, me);
                    }
                });
                RelativeLayout.LayoutParams params;
                params = new RelativeLayout.LayoutParams(
                        LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                params.leftMargin = deviceWidth * (4 + i * 10 + j % 2 * 5) / 101;
                params.topMargin = deviceWidth * (4 + 10 * j) / 101;
                params.width = deviceWidth * 8 / 101;
                params.height = deviceWidth * 8 / 101;
                blockList[a].setLayoutParams(params);
                rl.addView(blockList[a]);
            }
        }
    }
}

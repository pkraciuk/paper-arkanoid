package com.pkraciuk;

import android.content.Context;
import android.util.Log;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class BestScores {
    Context context;

    BestScores(Context context) {
        this.context = context;
    }
    public void saveScores(Player[] playerList) {
        try {
            FileOutputStream fos = context.openFileOutput("scores.data",Context.MODE_PRIVATE);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(playerList);
            os.close();
        } catch (Exception e) {
            Log.d("dodawanie", "cos sie wali przy zapisie");
        }
    }

    public Player[] loadScores() {
        Player[] playerList = null;
        try {
            FileInputStream fis = context.openFileInput("scores.data");
            ObjectInputStream is = new ObjectInputStream(fis);
            playerList = (Player[])is.readObject();
            is.close();
        } catch (Exception e) {
            playerList = new Player[6];
            playerList[0] = new Player("Quinn", "60");
            playerList[1] = new Player("Aiden", "50");
            playerList[2] = new Player("Kailey","40");
            playerList[3] = new Player("Sarah", "30");
            playerList[4] = new Player("Carol", "20");
            playerList[5] = new Player("Jaxon", "10");
        }
        //Log.d("dodawanie", "wczytanie bez problemow d");

        return playerList;
    }

}
